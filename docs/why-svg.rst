.. _why-svg:

========
Why SVG?
========

Or, in other words, why not another vector graphics format, like DXF,
DWG, PDF or Adobe Illustrator's?

- SVG is an **open standard** defined by the W3C

  - all modern browsers support directly visualising SVG
  - lots of vector graphics software support editing SVG

- SVG is XML, and this brings several advantages like

  - ability to do manual editing with a text editor
  - availability of advanced processing tools
  - ease of translation to other formats, both vector and raster

- SVG is part of official recommendations and guidelines:

  - it is one of the two *preferred* formats for vector graphics
    at the `Archaeology Data Service`_ (together with DWG)
  - it is the only recommended format for vector graphics
    in the `MINERVA EC Guidelines`_ for Digital Cultural Content Creation Programmes
  - unfortunately the Deutsches Archäologisches Institut
    still doesn't mention SVG
    in their `IT-Leitfaden`_ (recommendations)

.. _`Archaeology Data Service`: http://archaeologydataservice.ac.uk/advice/depositCreate2#section-depositCreate2-2.2.1.OverviewOfPreferredDataFormats
.. _`Minerva EC Guidelines`:  http://www.minervaeurope.org/interoperability/technicalguidelines.htm
.. _`IT-Leitfaden`: http://www.dainst.org/it/node/24270?ft=all

In this chapter,
we are first going to look
at some other popular formats,
with their limitations,
and then we will examine why
SVG is the best *available* choice
for publishing pottery on the Web.

Other Formats: Why Not ... ?
============================

There are several possible digital formats
for pottery drawings. Some are commonly used,
some are not, but all have in common one thing:
they are almost never used for publication
(with the exception of PDF),
where print or raster images are the current standard.

An example of raster images used to disseminate line drawings
of archaeological pottery is Roman Amphorae:
a digital resource (University of Southampton
2005), where a typological collection is
presented and for every type of amphora one or
more profiles are available as JPEG images.
In this section we discuss why most
formats are not suitable for Web publishing.

DWG
---

DWG is a proprietary format developed
by Autodesk Inc., and it is the native format
of the AutoCAD family of software programs.
AutoCAD is often used for digitising pottery
drawings, for several reasons. First, it is used
for other common tasks of archaeological
routine, such as site plans. Second, it has a
comprehensive set of drawing tools. Third,
AutoCAD guarantees good-quality printed
graphics. But DWG is not a good choice. The
first main problem with DWG is that most
often it is used, but not chosen. Rather, it just
happens to be the default format of a popular
software. The second problem is that DWG
is proprietary. Every version of AutoCAD
introduces a new version of the format,
incompatible with earlier versions. There is no
open source software capable of reading and
writing all DWG versions. No web browser has
or is likely to have in the future support for
DWG. As a consequence, DWG is not suitable
for being published on the Web.

DXF
---

DXF is an interoperable version of DWG.
Its specification is open (even though it remains
property of Autodesk Inc.) and there are several
open source libraries and programs capable of
reading and writing DXF. However, DXF files
tend to be very large in size compared to the
corresponding DWG files, and have the same
complex framework of incompatible versions.

PDF
---

PDF is an open standard, registered
with ISO and part of lots of recommendations
as format for archiving documents of many
different types. PDF is a vector format, that is,
vector graphics features (lines, polygons, text)
can be scaled retaining their quality. However,
objects contained in PDF are almost completely
unstructured: if three lines form a triangle, this
is not relevant in PDF, and therefore there is no
way to extract the information “this file contains
a triangle”. For this kind of purpose, pottery
drawings are no different than triangles. PDF
is suitable for archiving because it is easy to
create but generally not easy to edit and PDF
documents are usually intended to stay as
they are. Images can be included in PDF also
as raster images (with JPEG compression), so
using PDF for vector drawings is an ambiguous
recommendation. Moreover, PDF on the Web
is an ambiguous presence: despite being very
widespread, it is not natively supported by web
browsers (unless using additional plug-ins) and
it is treated as a downloadable file. PDF files
containing pottery drawings are for example
the ones published by the FOLD&R journal.

EPS/AI
------

EPS and AI are two variants developed
by Adobe Systems Inc. for their Illustrator
software. They share almost all problems of
PDF, and they are not native Web formats. To
make things more complicated, recent versions
of AI are proprietary and not interoperable.

GML
---

GML is an XML language and an open
standard of the Open Geospatial Consortium.
GML can be used to describe any spatial feature,
as commonly seen in GIS. Because all vector
spatial features are also geometric features,
it is possible to represent a vector graphics
object in GML. GML has two significant
limitations. First, it is not a vector graphics
format, and normal image viewing programs
are not able to deal with it. Second, it is deeply
rooted in the geospatial domain, and therefore
all data is assumed to be in a geospatial
coordinate reference system (CRS). If no CRS
is specified, WGS84 geographic coordinates
are assumed, and most importantly there is
no straightforward way of marking a GML file
as having a local, non-geographic coordinate
system.

SVG: strengths and limitations
------------------------------

Among the reasons why SVG is important in the specific case of pottery
drawings, some are significant for the purpose of sustainability.

First of all, SVG can be used for disseminating high-quality vector
graphics on the Web instead of raster images: in other words, this
means that drawings will have a quality equal or better than a 1:1
print, and it will be also possible to perform manual or automated
editing (e.g.  based on personal taste, or requirements from
publishers, colours and hatchings can be easily changed accordingly).

Secondly, SVG works across most vector graphics programs, and has a
rich community focused around the format and not the software. This
gives a higher sustainability to archives of SVG drawings: should one
program become obsolete or unmaintained, the migration path would be
comparatively easy, because no conversion of format would be
needed. Finally, dissemination on the Web is a good way to preserve
data, thanks to the existing mirroring services, and to the inevitable
downloads that will happen.

SVG is not without problems though, and
it should be clear that SVG is far from perfect
as a native format to create and archive pottery
drawings, while remaining the format of choice
for Web publishing.

The three main issues we could identify
testing an archive of several hundreds of drawings are:

- a lack of native support for units (i.e.  expressing the vector data
  in real units and not in pixels) can be worked around only with
  complex procedures, based on the ``<viewPort>`` element. To date,
  there is no straightforward way to deal with this issue, even though
  it could be addressed developing one or more plug-ins for the most
  used software programs;
- all programs have specific ways of dealing with image layers, that
  are not part of the native SVG 1.1 specification, but generally this
  is done through the ``<g>`` (*group*) element.  This is not a severe
  limitation, in that single objects can be assigned an id attribute,
  that is unique in each file. This possibility is discussed below in
  the advanced workflows section;
- except for basic elements such as ``<title>``, metadata can be expressed
  in different ways. While Inkscape uses Dublin Core and Creative
  Commons vocabularies, Adobe Illustrator follows the XMP
  standard. Both ways are acceptable and “standard” in their own way,
  but this discrepancy introduces another layer of complexity, making
  it sub- optimal to use different programs onto the same archive.

Finally, even though it is not strictly
a limitation, SVG is a file format, so the
expected structure of an archive is one file per
drawing, as with other image formats. Collating
more than one drawing in the same file is
discouraged, even though this is common when
preparing illustrations for print. In theory,
nothing prevents saving serialised SVG data in
a database [JYY2004]_,
to retain the relational structure often used in
archaeological information systems.

.. [JYY2004] W. Jianting, L. Yan and J. Youguang 2004 “A Design and
             Implementation of Spatial Database Based on XML-SVG”, in
             SVG Open 2004. 3rd Annual Conference on Scalable Vector
             Graphics.
             http://www.svgopen.org/2004/papers/ADesignandImplementationofSpatialDatabasebasedonxmlsvg/
