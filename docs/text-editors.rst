.. _text-editors:

==============
 Text editors
==============

Any text editor can be used for SVG source files. It's plain XML,
nothing else. Still, some editors are better than others.

Emacs
=====

`GNU Emacs`_ has very good support for SVG and it allows you to switch
between text mode and image mode.

.. _`GNU Emacs`: http://www.gnu.org/software/emacs/

Inkscape
========

Inkscape_ has a built-in XML tree viewer and editor.

.. _Inkscape: http://www.inkscape.org/

Other editors
=============

Lots of editors have XML editing capabilities, like tree-viewing and
source code highlighting.
