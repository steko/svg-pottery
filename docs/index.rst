.. SVG Pottery documentation master file, created by
   sphinx-quickstart on Sun May 29 12:06:18 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========================================
 SVG Pottery: pottery drawings on the Web
==========================================

SVG Pottery is a set of best practices, how-tos and recommendations
to ease the process of publishing pottery drawings on the Web.

In short  we strongly recommend that:

#. you publish your drawings on the Web, and not just on paper, not
   just inside a PDF article, but on their own
#. you use SVG as the format for publishing your drawings

The rest of this documentation goes into the details of why and how,
and explores some issues and possibilities with this model. You can
just go ahead and start using SVG if you like.

Some software tools are also being developed
to assist archaeologists
with simple, quick ways
to publish their drawings on the Web.

This documentation is maintained in a Git repository_ at Codeberg.

.. _repository: https://codeberg.org/steko/svg-pottery

If you want to cite this guide in your academic work,
you can refer to the following paper,
that was presented at the Computer Applications in Archaeology conference in 2012 [COSTA2014]_:

..  [COSTA2014] Costa, Stefano. 2014. «SVG Pottery: Upgrading Pottery
    Publications to the Web Age». In Archaeology in the Digital Era:
    Papers from the 40th Annual Conference of Computer Applications
    and Quantitative Methods in Archaeology (CAA), Southampton, 26-29
    March 2012, edited by Philip Verhagen, 533–40. Amsterdam
    University Press. http://dare.uva.nl/aup/en/record/500958

The SVG version described here is `SVG 1.1`_,
the current W3C recommendation.

.. _`SVG 1.1`: http://www.w3.org/TR/SVG11/

Contents:

.. toctree::
   :maxdepth: 2

   why-svg
   workflow
   issues
   text-editors
   software
   svg-usage-archaeology
   glossary

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
