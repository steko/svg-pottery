.. _svg-usage-archaeology:

=======================================
 Other use cases of SVG in archaeology
=======================================

SVG is already being used in a few archaeological environments, and there are
some cases that are worth mentioning here:

- Oxford Archaeology guide to plotting site plans using QGIS_ and then
  Inkscape for the advanced graphical editing of the layout [OAL366]_
  -- even though the focus is not on SVG as a format but on vector
  graphics;
- an interactive image viewer on the Web was developed for the Sikyon
  Survey Project using Scalable Vector Graphics (SVG) and Ajax
  [CHARNO2007]_;
- SVG as a potential tool for archaeologists is discussed in
  [WRIGHT2006]_, including some of the ways vector graphics are used
  in archaeology, and outlines the development and features of SVG,
  which are then demonstrated in the form of a case study using large-scale
  plan and section drawings converted to SVG from AutoCAD;
- a combined approach using SVG adn X3D to record and visualise
  archaeological textiles is presented in [PAGI2010]_, offering a
  detailed overview of how SVG is used.

Most of these cases share a common focus on site plans, or spatial data in
general. The tendency of spatial archaeology to be very advanced in
the use of information technology, and the wide gap that separes other
subdomains in this respect, is beyond the scope of this document.

There is indeed one example of SVG for pottery drawings, in the
excellent “Greek, Roman and Byzantine Pottery at Ilion” web
publication ([GRBPILION]_). You can not only browse the extensive
catalogue of pottery finds from the excavation, but also download the
entire site as a compressed archive. Inside the compressed archive the
JPEG images of pottery drawings come along with their SVG
originals. Actually, GRBP is one of the inspirations for this
research. Their approach is purely graphical, meaning that there is no
care for the SVG source and its content, as long as it renders to a
proper image. There is no grouping of graphic entities based on their
meaning within the drawing. The scale is given by a graphical 5 cm
scale, that is included in every file.

As suggested in the quickstart of this documentation, this is a very
good example for those who don't want to go into the details of SVG
and just care about using a standard, open format for publishing their
work.

.. _QGIS: http://www.qgis.org/

.. [OAL366] C. Robinson, D. Campbell and A. Hodgkinson, 2010
   “Archaeological maps from qGIS and Inkscape: A brief
   guide”. Documentation. Oxford Archaeology North. (Unpublished)
   http://library.thehumanjourney.net/366/

.. [CHARNO2007] M. Charno 2007, “An Interactive Image Using SVG and Ajax in Archaeology”,
   *Internet Archaeology*, 23 http://intarch.ac.uk/journal/issue23/charno_index.html

.. [WRIGHT2006] H. Wright 2006, “Archaeological Vector Graphics and SVG: A case
   study from Cricklade”, *Internet Archaeology*, 20
   http://intarch.ac.uk/journal/issue20/wright_index.html

.. [GRBPILION] S. Heath and B. Tekkök (eds.), 2006-2009, “Greek, Roman
   and Byzantine Pottery at Ilion (Troia)”. Retrieved 2011-06-02 from
   http://classics.uc.edu/troy/grbpottery/

.. [PAGI2010] H. Pagi, 2010, “When data becomes information:
	      visualizing archaeological textiles” in *Computer
	      Applications and Quantitative Methods in Archaeology,
	      Williamsburg, US*, Oxford, GB, Archaeopress,
	      285-291. http://eprints.soton.ac.uk/161699/
