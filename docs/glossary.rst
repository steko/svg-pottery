.. _glossary:

==========
 Glossary
==========

.. glossary:: 
   :sorted:

   drawing
	a drawing is a formal representation of a pot(sherd), intented
	for the purpose of publishing the exact shape with an optimal
	use of space on printed paper.

   axis of simmetry
   	if present, the axis of simmetry is usually taken not only to
   	represent the axis of rotation of the pot, but also often to
   	split the drawing in two parts: on one side, a section is
   	provided, as if the pot was cut along the radius; one the
   	opposite side, a profile from outside is shown. There is no
   	accepted standard of which side should be which.

   DOM
	the Document Object Model is a technical way to describe the
	“tree” of nested XML tags that compose all documents,
	including SVG. If a software program is able to manipulate the
	DOM, this means it could do something useful for you, like
	filtering out certain parts of the drawing based on attributes.
