.. _software:

=============================
 Software for SVG processing
=============================

Digitisation
============

This is a non-comprehensive list
of vector graphic programs
that can be used to digitise your pottery drawings,
and subsequently save them to SVG.

From the point of view of *SVG Pottery*,
the most important feature
is the quality of the resulting SVG file,
in terms of tidy source and metadata preservation.

Inkscape
--------

Inkscape_ is a vector drawing software,
available under the GNU GPL license,
for all major operating systems.

Inkscape uses SVG as its default format,
but it has some additional features
compared to the standard SVG specification.

Inkscape can also edit metadata of SVG documents,
like title, description, creator and licensing.

.. _Inkscape: http://www.inkscape.org/

Dealing with units in Inkscape
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SVG is far from ideal
for handling real-world units like millimeters.
Some useful advice for dealing with the limitations
when using Inkscape
can be found here:

- http://inkscape.13.x6.nabble.com/Units-in-Inkscape-td4971507.html
- http://wiki.inkscape.org/wiki/index.php/Units_In_Inkscape


Adobe Illustrator
-----------------

`Adobe Illustrator`_ is a proprietary vector drawing software,
available from Adobe in the *Creative Suite* pack.

Illustrator has its own proprietary format,
but can import and export SVG natively.

If you use Illustrator for your drawings,
you can easily save them to SVG.

The basic steps to follow, besides the drawing itself, are:

- add metadata (author, title, description)
  via the :menuselection:`File --> File info` menu item
- :menuselection:`File --> Save as...`
  and choose SVG as format
- the SVG Options dialog is very important:

  - be sure to enable
    the *Include XMP* option,
    that will include metadata
    in the SVG file
  - use the *Show SVG Code* option
    to have a preview
    of the generated SVG
  - stick to SVG 1.1 (default)

.. _`Adobe Illustrator`: http://www.adobe.com/products/illustrator.html

Other programs
--------------

Other vector graphics programs have poor or no support for SVG:

- Xara Xtreme: has no support for SVG
- Corel Draw: *unknown* 

Editing and automation
======================

The following tools
are for automated editing and processing
of SVG files.

Their use is suited for advanced users.

Batik
-----

Batik_ is a complete SVG implementation for Java developers.
It also has a few small tools
that could be useful
when post-processing your drawings,
like:

- ``rasterizer``, an SVG-to-bitmap converter
- ``svgpp``, a pretty printer for SVG source code
- ``squiggle``, a visualization program

.. _Batik: http://xmlgraphics.apache.org/batik/

Ink Generator
-------------

ink-generator is an Inkscape extension
to substitute text and data
in automatically-generated files,
starting from an SVG model
and a CSV data file.

Scour
-----

Scour is a Python module
that aggressively cleans SVG files,
removing a lot of unnecessary information
that certain tools or authors embed into their documents.
The goal of scour is to provide
an identically rendered image
(i.e. a scoured document should have
no discernable visible differences from the original file)
while minimizing the file size.

.. warning::

   Scour is intended to be run on files
   that have been edited in Vector Graphics editors
   such as Inkscape or Adobe Illustrator.
   Scour attempts to optimize the file,
   and as result,
   it will change the file's structure and (possibly) its semantics.
   If you have hand-edited your SVG files,
   you will probably not be happy
   with the output of Scour.

   Never use scour to overwrite your original file!

xmldiff
-------

xmldiff_ is a Python tool
that shows the differences
between two similar XML files.

.. _xmldiff: http://www.logilab.org/project/xmldiff
